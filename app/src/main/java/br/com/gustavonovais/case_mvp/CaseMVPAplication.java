package br.com.gustavonovais.case_mvp;

import android.app.Application;
import android.content.Context;

import com.activeandroid.ActiveAndroid;
import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by Gustavo on 14/03/17.
 */

public class CaseMVPAplication extends Application {

    public static Context context;

    public CaseMVPAplication() {
        context = this;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
        Fresco.initialize(this);
    }
}
