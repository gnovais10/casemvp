package br.com.gustavonovais.case_mvp.adapter;

import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;


import com.facebook.drawee.generic.RoundingParams;

import java.util.List;

import br.com.gustavonovais.case_mvp.R;
import br.com.gustavonovais.case_mvp.databinding.RowBinding;
import br.com.gustavonovais.case_mvp.data.UserDb;
import br.com.gustavonovais.case_mvp.util.DateUtils;

public class AdapterUsers extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final List<UserDb> dataList;

    public AdapterUsers(List<UserDb> dataList) {
        this.dataList = dataList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder myViewHolder = (ViewHolder) holder;
            myViewHolder.bindItem(position);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final RowBinding binding;


        public ViewHolder(RowBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindItem(final int position) {
            UserDb data = dataList.get(position);

            binding.txtName.setText(data.name);
            binding.txtBio.setText(data.bio);
            binding.image.setImageURI(data.image);
            binding.txtBirthday.setText(DateUtils.dateFormated(data.birthday));
            configImg(this, data);
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private void configImg(ViewHolder viewHolder, UserDb user) {
        Uri uri = Uri.parse(user.image);
        viewHolder.binding.image.setImageURI(uri);

        RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
        roundingParams.setRoundAsCircle(true);
        viewHolder.binding.image.getHierarchy().setRoundingParams(roundingParams);
    }


}
