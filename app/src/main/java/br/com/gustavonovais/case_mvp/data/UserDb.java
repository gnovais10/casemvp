package br.com.gustavonovais.case_mvp.data;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by Gustavo on 14/03/17.
 */

@Table(name = "USERS")
public class UserDb extends Model {

    @Column(name = "id_", unique = true)
    public String id;

    @Column(name = "name")
    public String name;

    @Column(name = "image")
    public String image;

    @Column(name = "birthday")
    public String birthday;

    @Column(name = "bio")
    public String bio;

    public static List<UserDb> getAll() {
        return new Select()
                .from(UserDb.class)
                .execute();
    }

    public static UserDb getById(String name) {
        return new Select()
                .from(UserDb.class)
                .where("id_ = ?", name)
                .executeSingle();
    }
}
