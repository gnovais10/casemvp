package br.com.gustavonovais.case_mvp.di;

import br.com.gustavonovais.case_mvp.home.HomeModel;
import br.com.gustavonovais.case_mvp.home.HomePresenter;
import br.com.gustavonovais.case_mvp.home.HomeView;
import dagger.Component;

/**
 * Created by GustavoNovais on 23/03/17.
 */

@Component(modules = HomeModule.class)
public interface HomeComponent {

    public HomePresenter getHomePresenter();

    public HomeModel getHomeModel();

    void inject(HomeView homeView);

}
