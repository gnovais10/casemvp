package br.com.gustavonovais.case_mvp.di;

import br.com.gustavonovais.case_mvp.home.HomeModel;
import br.com.gustavonovais.case_mvp.home.HomePresenter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by GustavoNovais on 23/03/17.
 */

@Module
public class HomeModule {

    @Provides
    public HomeModel providesHomeModel() {
        return new HomeModel();
    }

    @Provides
    public HomePresenter providesHomePresenter(HomeModel homeModel) {
        return new HomePresenter(homeModel);
    }
}
