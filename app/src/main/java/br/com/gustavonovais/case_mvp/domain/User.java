package br.com.gustavonovais.case_mvp.domain;

/**
 * Created by Gustavo on 14/03/17.
 */

public class User {

    public String id;
    public String name;
    public String image;
    public String birthday;
    public String bio;

}
