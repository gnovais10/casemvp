package br.com.gustavonovais.case_mvp.home;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Conductor;
import com.bluelinelabs.conductor.Controller;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;

import br.com.gustavonovais.case_mvp.HomeBinding;
import br.com.gustavonovais.case_mvp.R;
import br.com.gustavonovais.case_mvp.di.DaggerHomeComponent;
import br.com.gustavonovais.case_mvp.di.HomeComponent;

public class HomeActivity extends AppCompatActivity {

    public static HomeComponent homeComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HomeBinding homeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        setSupportActionBar(homeBinding.toolbar);

        homeComponent = DaggerHomeComponent.builder().build();

        setController(savedInstanceState);

    }

    public void setController(Bundle savedInstanceState) {
        ViewGroup container = (ViewGroup) findViewById(R.id.controller_container);
        Router router = Conductor.attachRouter(this, container, savedInstanceState);

        if (!router.hasRootController()) {
            Controller controller = new HomeView();
            router.setRoot(RouterTransaction.with(controller));
        }
    }

    public static HomeComponent getHomeComponent() {
        return homeComponent;
    }
}
