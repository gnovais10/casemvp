package br.com.gustavonovais.case_mvp.home;

import android.os.Bundle;

import br.com.gustavonovais.case_mvp.domain.User;

/**
 * Created by Gustavo on 21/03/17.
 */

public interface HomeContract {

    interface Presenter {
        void saveUsersBD();
    }

    interface View {
        void setRecyclerView();
        void configOnRefresh();
    }

}
