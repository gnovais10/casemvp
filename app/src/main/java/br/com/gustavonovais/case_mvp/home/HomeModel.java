package br.com.gustavonovais.case_mvp.home;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.gustavonovais.case_mvp.data.UserDb;
import br.com.gustavonovais.case_mvp.domain.User;
import br.com.gustavonovais.case_mvp.util.Util;

/**
 * Created by Gustavo on 21/03/17.
 */

public class HomeModel  {

    public HomeModel( ){
    }

    public void saveUsersBD() {
        User[] usersList = jsonToListUsers();

        if (usersList != null){
            for(User users1 : usersList){
                UserDb userBD = new UserDb();
                userBD.id = users1.id;
                userBD.name = users1.name;
                userBD.image = users1.image;
                userBD.birthday = users1.birthday;
                userBD.bio = users1.bio;

                if(UserDb.getById(userBD.id) == null){
                    userBD.save();
                }
            }
        }
    }

    public User[] jsonToListUsers() {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(readUsers(), User[].class);
    }

    public String readUsers() {
        return Util.readJSONFromAsset();
    }
}
