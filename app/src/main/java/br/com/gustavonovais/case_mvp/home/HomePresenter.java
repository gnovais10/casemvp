package br.com.gustavonovais.case_mvp.home;

import android.os.Bundle;

import javax.inject.Inject;

import br.com.gustavonovais.case_mvp.domain.User;

/**
 * Created by Gustavo on 21/03/17.
 */

public class HomePresenter implements HomeContract.Presenter {

    @Inject
    HomeModel homeModel;

    public HomePresenter(HomeModel homeModel) {
        this.homeModel = homeModel;
    }

    @Override
    public void saveUsersBD() {
        homeModel.saveUsersBD();
    }

}
