package br.com.gustavonovais.case_mvp.home;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Controller;


import javax.inject.Inject;

import br.com.gustavonovais.case_mvp.HomeControllerBinding;
import br.com.gustavonovais.case_mvp.R;
import br.com.gustavonovais.case_mvp.adapter.AdapterUsers;
import br.com.gustavonovais.case_mvp.data.UserDb;

/**
 * Created by GustavoNovais on 27/01/17.
 */

public class HomeView extends Controller implements SwipeRefreshLayout.OnRefreshListener, HomeContract.View {

    private HomeControllerBinding binding;

    @Inject
    HomePresenter presenter;

    public HomeView() {
    }

    @NonNull
    @Override
    protected android.view.View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        binding = DataBindingUtil.inflate(inflater, R.layout.home_controller, container, false);
        android.view.View view = binding.getRoot();

        HomeActivity.getHomeComponent().inject(this);
        presenter.saveUsersBD();

        setRecyclerView();
        configOnRefresh();

        return view;
    }

    @Override
    public void configOnRefresh(){
        binding.swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void setRecyclerView() {
        AdapterUsers adapter = new AdapterUsers(UserDb.getAll());
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.getAdapter().notifyDataSetChanged();
        binding.swipeRefreshLayout.setRefreshing(false);
    }


    @Override
    public void onRefresh() {
        binding.swipeRefreshLayout.setRefreshing(true);
        setRecyclerView();
    }
}
