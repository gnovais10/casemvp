package br.com.gustavonovais.case_mvp.util;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/**
 * Created by gustavo.silva on 04/11/16.
 */

public class DateUtils {

    private static final String format = "dd/MM/yyyy";

    public static String dateFormated(String dateIso){
        DateTime dateTime = convertIsoToDate(dateIso);
        DateTimeFormatter fmt = DateTimeFormat.forPattern(format);
        return  fmt.print(dateTime);
    }

    public static DateTime convertIsoToDate(String dateIso){
        return ISODateTimeFormat.dateTimeNoMillis().parseDateTime(dateIso);
    }

}