package br.com.gustavonovais.case_mvp.util;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.io.IOException;
import java.io.InputStream;

import static br.com.gustavonovais.case_mvp.CaseMVPAplication.context;


/**
 * Created by Gustavo on 14/03/17.
 */

public class Util {

    public static String readJSONFromAsset() {
        String json = null;
        try {
            InputStream is = context.getAssets().open("users.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public DateTime convertIsoToDate(){
        String startDate = "2013-07-12T18:31:01.000Z";
        return ISODateTimeFormat.dateTime().parseDateTime(startDate);
    }
}
